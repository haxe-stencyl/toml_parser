package ;

import toml.TomlParser;

class Main {
    public static function main() {
        trace("lalala");

	var m = new TomlParser();
	var test = myTest; 
	var mm = m.loadFromString(test);
	trace(test);
	trace(mm);

	trace(mm.getValue());
    }


static var myTest = "
[Nuit]
1 = \"J'ai envie de me coucher\"
2 = \"J'ai fait un cauchemard\"
3 = \"J'ai soif\"
4 = \"J'ai mal\"
5 = \"J'ai du mal à dormir\"

[Greetings]
1 = \"Bonjour, je m'appelle Maud\"
2 = \"Comment vous appelez-vous\"
3 = \"Bonjour\"
4 = \"Enchanté\"




";
    /*
	TODO
	 mutilines quotes
	 simple quotes doubles quotes,
	 escape quote


	 utf8
	 endLine
	 [a.a]
	 write inline tables like you wish

	 https://github.com/toml-lang/toml/blob/master/toml.md#user-content-array





    */


    // Comments
    /*
    if strict typing 
    key2 = # INVALID  , key is invalidated


     */

    static var test_comments = "
# This is a full-line comment
key = \"value\"  # This is a comment at the end of a line
key = \"value\"  # This is a comment at the end of a line
another = \"# This is not a comment\"
another2 = \"# This is not a= comment\"
key2 = # INVALID
first = \"Tom\" last = \"Preston-Werner\" # INVALID
    ";


static var quoted_keys= "
\"127.0.0.1\" = \"value\"
\"character encoding\" = \"value\"
'key2' = \"value\"
'quoted \"value\"' = \"value\"



";

static var hard ="
 test_array = [ \"] \", \" # \"] # ] There you go, parse this!
   
    # You didn't think it'd as easy as chucking out the last #, did you?
    another_test_string = \" Same thing, but with a string #\"
    harder_test_string = \" And when \"'s are in the string, along with # \"\" # \"and comments are there too\"

";

static var example = "
title = \"TOML Example\"

name = { first = \"Tom\", \"last\" = \"Preston-Werner\" }

[owner]
name = \"Tom Preston-Werner\"


#trytrytrtry
[database] #bkjhkhj
server = \"192.168.1.1\"
ports = [ 8001 , \"ezrez\", 8001 , 8002 ]
connection_max = 5000
enabled = true";
/*
mult

dob = 1979-05-27T07:32:00-08:00 # First class dates
[servers]

  # Indentation (tabs and/or spaces) is allowed but not required
  [servers.alpha]
  ip = \"10.0.0.1\"
  dc = \"eqdc10\"

  [servers.beta]
  ip = \"10.0.0.2\"
  dc = \"eqdc10\"

[clients]
data = [ [\"gamma\", \"delta\"], [1, 2] ]

# Line breaks are OK when inside arrays
hosts = [
  \"alpha\",
  \"omega\"
]
";
/*
'' = 'blank' 

\"ʎǝʞ\" = \"value\"


"*/
//A bare key must be non-empty, but an empty quoted key is allowed (though discouraged).
/*
= \"no key name\"  # INVALID
\"\" = \"blank\"     # VALID but discouraged
'' =  \"blank\"  # VALID but discouraged

*/

static var bare_keys = "
= \"no key name\"  # INVALID

";

static var dotted_keys = "
fruit.name = \"banana\"     # this is best practice
fruit. color = \"yellow\"    # same as fruit.color
fruit . flavor = \"banana\"   # same as fruit.flavor
";

// For now must finish by a new line
}