# TOML Parser 





You can read about TOML here https://github.com/toml-lang/toml



or here https://toml.io/en/

```toml


# This is a TOML document.

title = "TOML Example"

[database]
server = "192.168.1.1"
ports = [ 8000, 8001, 8002 ]
connection_max = 5000
enabled = true

[servers]

  # Indentation (tabs and/or spaces) is allowed but not required
  [servers.alpha]
  ip = "10.0.0.1"
  dc = "eqdc10"

  [servers.beta]
  ip = "10.0.0.2"
  dc = "eqdc10"

[clients]
data = [ ["gamma", "delta"], [1, 2] ]

# Line breaks are OK when inside arrays
hosts = [
  "alpha",
  "omega"
]
```





## Difference with official Specs

- Dates do not work

- In arrays, trailing commas don't work

  a = ["e", 4] OK

  a = ["e", 4,] NOT YET

- In arrays, only one double quotes list work 

  a = ["aa","ee"] OK

  a = ['aa',"""eee""", '''eee'''] NOT YET


## 

```haxe
 // For now you cannot set any options for parsing, but hopefully you will be able soon
  var tomlSection =  new TomlParser().loadFromString("
  [SectionName]
  a = 'a'
  ");  


```

## TODO Soon

- to be able to write toml back to a string



## Improvements I want to make  but not soon

- accept ";" comments for total compatibility with .ini files
- give warnings when it doesn't obey to strict TOML guidelines
- be able to write comments dynamically to a toml file.
- who knows, maybe Macros ???



## Stencyl Blocks



![snippetcode](doc/snippetcode.png)



will transform the previous toml text to a map.

```haxe
{title => TOML Example, database => {server => 192.168.1.1, ports => [8000,8001,8002], connection_max => 5000, enabled => true}, servers => {alpha => {ip => 10.0.0.1, dc => eqdc10}, beta => {ip => 10.0.0.2, dc => eqdc10}}, clients => {data => [[gamma,delta],[1,2]], hosts => [alpha,omega]}}
```



