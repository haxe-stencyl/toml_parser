package tests;

import utest.Assert;
import utest.Async;
import toml_parser.TomlParser;
#if sys
import sys.io.File;
#end

class TestCaseTomlSpecs extends utest.Test {
	  public function setup() {

  	}

	@Ignored
	function testBareKeys(){
	var s = 
'key = "value"
bare_key = "value"
bare-key = "value"
1234 = "value"
';
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	trace(mm.getValue());
	Assert.equals(mm.getValue().get("key"), "value");
	Assert.equals(mm.getValue().get("bare_key"), "value");
	Assert.equals(mm.getValue().get("bare-key"), "value");
	Assert.equals(mm.getValue().get("1234"), "value");
	
}
	@Ignored
	function testQuotedKeys(){
	var s ="
\"127.0.0.1\" = \"value\"
\"character encoding\" = \"value\"
\"ʎǝʞ\" = \"value\"
'key2' = \"value\"
'quoted \"value\"' = \"value\"
";
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("127.0.0.1"), "value");
	Assert.equals(mm.getValue().get("character encoding"), "value");
	Assert.equals(mm.getValue().get("ʎǝʞ"), "value");
	Assert.equals(mm.getValue().get("key2"), "value");
	Assert.equals(mm.getValue().get("quoted \"value\""), "value");
	trace(mm.getValue());
}
	@Ignored
	function testDottedKeys(){
	var s ='
name = "Orange"
physical.color = "orange"
physical.shape = "round"
site."google.com" = true
fruit.name = "banana"     # this is best practice
fruit. color = "yellow"    # same as fruit.color
fruit . flavor = "banana"   # same as fruit.flavor
apple.type = "fruit"
orange.type = "fruit"

apple.skin = "thin"
orange.skin = "thick"

apple.color = "red"
orange.color = "orange"
';
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("name"), "Orange");
	Assert.equals(mm.getValue().get("physical").get("color"), "orange");
	Assert.equals(mm.getValue().get("physical").get("shape"), "round");
	Assert.equals(mm.getValue().get("site").get("google.com"), true);
	Assert.equals(mm.getValue().get("fruit").get("name"), "banana");
	Assert.equals(mm.getValue().get("fruit").get("color"), "yellow");
	Assert.equals(mm.getValue().get("fruit").get("flavor"), "banana");
}

	@Ignored
	function testOutOfOrderDottedKeys(){
	var s ='
apple.type = "fruit"
orange.type = "fruit"

apple.skin = "thin"
orange.skin = "thick"

apple.color = "red"
orange.color = "orange"
';
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("apple").get("type"), "fruit");
	Assert.equals(mm.getValue().get("orange").get("type"), "fruit");
	Assert.equals(mm.getValue().get("apple").get("skin"), "thin");
	Assert.equals(mm.getValue().get("orange").get("skin"), "thick");
	Assert.equals(mm.getValue().get("apple").get("color"), "red");
	Assert.equals(mm.getValue().get("orange").get("color"), "orange");
}


	function testInts(){
	var s=
"int1 = +99
int2 = 42
int3 = 0
int4 = -17
int5 = 1_000
int6 = 5_349_221
int7 = 53_49_221  # Indian number system grouping
int8 = 1_2_3_4_5  # VALID but discouraged
hex1 = 0xDEADBEEF
hex2 = 0xdeadbeef
hex3 = 0xdead_beef

# octal with prefix `0o`
oct1 = 0o01234567
oct2 = 0o755 # useful for Unix file permissions

# binary with prefix `0b`
bin1 = 0b11010110

# not a number
sf4 = nan  # actual sNaN/qNaN encoding is implementation-specific
sf5 = +nan # same as `nan`
sf6 = -nan # valid, actual encoding is implementation-specific
";
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("int1"), 99);
	Assert.equals(mm.getValue().get("int2"), 42);
	Assert.equals(mm.getValue().get("int3"), 0);
	Assert.equals(mm.getValue().get("int4"), -17);
	Assert.equals(mm.getValue().get("int5"), 1000);
	Assert.equals(mm.getValue().get("int6"), 5349221);
	Assert.equals(mm.getValue().get("int7"), 5349221);
	Assert.equals(mm.getValue().get("int8"), 12345);
	Assert.equals(mm.getValue().get("hex1"), 0xDEADBEEF);
	Assert.equals(mm.getValue().get("hex2"), 0xdeadbeef);
	Assert.equals(mm.getValue().get("hex3"), 0xdeadbeef);
	Assert.equals(mm.getValue().get("oct1"), 342391);
	Assert.equals(mm.getValue().get("oct2"), 493);
	Assert.equals(mm.getValue().get("bin1"), 214);


}

function testFloats(){
	var s=
"
# fractional
flt1 = +1.0
flt2 = 3.1415
flt3 = -0.01

# exponent
flt4 = 5e+22
flt5 = 1e06
flt6 = -2E-2

# both
flt7 = 6.626e-34
flt8 = 224_617.445_991_228

# infinity
sf1 = inf  # positive infinity
sf2 = +inf # positive infinity
sf3 = -inf # negative infinity

# not a number
sf4 = nan  # actual sNaN/qNaN encoding is implementation-specific
sf5 = +nan # same as `nan`
sf6 = -nan # valid, actual encoding is implementation-specific


";
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("flt1"), 1);
	Assert.equals(mm.getValue().get("flt2"), 3.1415);
	Assert.equals(mm.getValue().get("flt3"), -0.01);
	Assert.equals(mm.getValue().get("flt4"), 5e+22);
	Assert.equals(mm.getValue().get("flt5"), 1e06);
	Assert.equals(mm.getValue().get("flt6"), -2E-2);
	Assert.equals(mm.getValue().get("flt7"), 6.626e-34);
	Assert.equals(mm.getValue().get("flt8"), 224617.445991228);
	Assert.equals(mm.getValue().get("sf1"), Math.POSITIVE_INFINITY);
	Assert.equals(mm.getValue().get("sf2"), Math.POSITIVE_INFINITY);
	Assert.equals(mm.getValue().get("sf3"), Math.NEGATIVE_INFINITY);


}

function testBoolean(){
	var s=
"
bool1 = true
bool2 = false
";
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("bool1"), true);
	Assert.equals(mm.getValue().get("bool2"), false);

}

function testDate(){
	var s=
"
ld1 = 1979-05-27
lt1 = 07:32:00
lt2 = 00:32:00.999999
";
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("bool1"), true);
	Assert.equals(mm.getValue().get("bool2"), false);

}
function testStrings(){
	var s=
'
str = "I\'m a string. \\\"You can quote me\\\". Name\tJos\u00E9\nLocation\tSF."
str10 = """
Roses are red
Violets are blue"""

# The following strings are byte-for-byte equivalent:
str1 = "The quick brown fox jumps over the lazy dog."

str2 = """
The quick brown \\


  fox jumps over \\
    the lazy dog.\\
"""
str3 = """\\
       The quick brown \\
       fox jumps over \\
       the lazy dog.\\
       """

str4 = """Here are two quotation marks: "". Simple enough."""
# str5 = """Here are three quotation marks: """."""  # INVALID
str5 = """Here are three quotation marks: ""\\"."""
str6 = """Here are fifteen quotation marks: ""\\"""\\"""\\"""\\"""\\"."""

# "This," she said, "is just a pointless statement."
str7 = """"This," she said, "is just a pointless statement.""""

';
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("str"), 'I\'m a string. "You can quote me". Name\tJos\u00E9\nLocation\tSF.');
	Assert.equals(mm.getValue().get("str10"), "Roses are red\nViolets are blue");
	Assert.equals(mm.getValue().get("str1"), "The quick brown fox jumps over the lazy dog.");
	Assert.equals(mm.getValue().get("str2"), "The quick brown fox jumps over the lazy dog.");
	Assert.equals(mm.getValue().get("str3"), "The quick brown fox jumps over the lazy dog.");
	Assert.equals(mm.getValue().get("str4"), 'Here are two quotation marks: "". Simple enough.');
	Assert.equals(mm.getValue().get("str5"), 'Here are three quotation marks: """.');
	Assert.equals(mm.getValue().get("str6"), 'Here are fifteen quotation marks: """"""""""""""".');
	Assert.equals(mm.getValue().get("str7"), '"This," she said, "is just a pointless statement."', "problems with more than 3 quotations makrs in a row");
}


function testLiteralStrings(){
	var s= File.getContent("literal_strings.toml");
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(mm.getValue().get("winpath"), 'C:\\Users\\nodejs\\templates');
	Assert.equals(mm.getValue().get("winpath2"), '\\\\ServerX\\admin$\\system32\\');
	Assert.equals(mm.getValue().get("quoted"), 'Tom "Dubs" Preston-Werner');
	Assert.equals(mm.getValue().get("regex"), '<\\i\\c*\\s*>');
	Assert.equals(mm.getValue().get("regex2"), "I [dw]on't need \\d{2} apples");
	Assert.equals(mm.getValue().get("lines"), "The first newline is
trimmed in raw strings.
   All other whitespace
   is preserved.
");

	Assert.equals(mm.getValue().get("quot15"), 'Here are fifteen quotation marks: """""""""""""""');
	Assert.equals(mm.getValue().get("apos15"), "Here are fifteen apostrophes: '''''''''''''''");
	Assert.equals(mm.getValue().get("strs"), "'That,' she said, 'is still pointless.'");
}
function testArrays(){
	var s= 
"

colors = [ \"red\", \"yellow\", \"green\" ]
nested_arrays_of_ints = [ [ 1, 2 ], [3, 4, 5] ]
nested_mixed_array = [ [ 1, 2 ], [\"a\", \"b\", \"c\"] ]
numbers = [ 0.1, 0.2, 0.5, 1, 2, 5 ]
string_array = [ \"all\", 'strings', \"\"\"are the same\"\"\", '''type''' ]
contributors = [
  \"Foo Bar <foo@example.com>\",
  { name = \"Baz Qux\", email = \"bazqux@example.com\", url = \"https://example.com/bazqux\" }
]
integers2 = [
  1, 2, 3
]

integers3 = [
  1,
  2, # this is ok
]
";
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.equals(""+mm.getValue().get("colors"), "[red,yellow,green]");
	Assert.equals(""+mm.getValue().get("nested_arrays_of_ints"), "[[1,2],[3,4,5]]");
	Assert.equals(""+mm.getValue().get("nested_mixed_array"), "[[1,2],[a,b,c]]");
	Assert.equals(""+mm.getValue().get("numbers"), "[0.1,0.2,0.5,1,2,5]");
	Assert.equals(""+mm.getValue().get("string_array"), "[all,strings,are the same, type]");//, "triple comments don't work");
	Assert.equals(""+mm.getValue().get("integers2"), "[1,2,3]");
	Assert.equals(""+mm.getValue().get("integers3"), "[1,2]", "trailing commas don't work");
}


function testTable(){
	var s=
"
[table-1]
key1 = \"some string\"
key2 = 123

[table-2]
key1 = \"another string\"
key2 = 456

[dog.\"tater.man\"]
type.name = \"pug\"

[a.b.c]            # this is best practice
[ d.e.f ]          # same as [d.e.f]
[ g .  h  . i ]    # same as [g.h.i]

[fruit]
apple.color = \"red\"
apple.taste.sweet = true

# [fruit.apple]  # INVALID
# [fruit.apple.taste]  # INVALID

[fruit.apple.texture]  # you can add sub-tables
smooth = true

";

//[ j . "ʞ" . 'l' ]  # same as [j."ʞ".'l']
	var m = new TomlParser(); 
	var mm = m.loadFromString(s);
	Assert.isOfType(mm.getSectionByName("table-1"),TOMLSection);
	Assert.isOfType(mm.getSectionByName("table-2"),TOMLSection);
	Assert.isOfType(mm.getSectionByName("dog"),TOMLSection);
	Assert.isOfType(mm.getSectionByName("dog").getSectionByName("tater.man"),TOMLSection);
	Assert.isOfType(mm.getSectionByName("a", false).getSectionByName("b",false).getSectionByName("c",false),TOMLSection);
	Assert.isOfType(mm.getSectionByName("d", false).getSectionByName("e",false).getSectionByName("f",false),TOMLSection);
	Assert.isOfType(mm.getSectionByName("g", false).getSectionByName("h",false).getSectionByName("i",false),TOMLSection);
}


  //asynchronous teardown
  @:timeout(700) //default timeout is 250ms
  public function teardown(async:Async) {
    //field = null; // not really needed

    //simulate asynchronous teardown
    haxe.Timer.delay(
      function() {
        //resolve asynchronous action
        async.done();
      },
      500
    );
  }
}
