package tests;

import utest.Assert;
import utest.Async;
import toml_parser.TomlParser;

class TestCaseSimple extends utest.Test {

/*
function testComplete(){

	var test_complete =' [database] server = "192.168.1.1" ports = [ 8000, 8001, 8002 ] connection_max = 5000 enabled = true  [servers]    # Indentation (tabs and/or spaces) is allowed but not required   [servers.alpha]   ip = "10.0.0.1"   dc = "eqdc10"    [servers.beta]   ip = "10.0.0.2"   dc = "eqdc10"  [clients] data = [ ["gamma", "delta"], [1, 2] ]  # Line breaks are OK when inside arrays hosts = [   "alpha",   "omega" ]';


}
*/



	function testComments(){
		var test_comments = "
# This is a full-line comment
key = \"value\"  # This is a comment at the end of a line
another = \"# This is not a comment\"
another2 = \"# This is not a= comment\"
";
		var m = new TomlParser(); 
		var mm = m.loadFromString(test_comments);
		Assert.equals(Lambda.count(mm.getValue()), 3);
		Assert.equals(mm.getValue().get("key"), "value");
		Assert.equals(mm.getValue().get("another"), "# This is not a comment");
		Assert.equals(mm.getValue().get("another2"), "# This is not a= comment");
	}


	function testQuotedKeys(){
		var quoted_keys= "
[quoted_keys]
\"127.0.0.1\" = \"value\"
\"character encoding\" = \"value\"
'key2' = \"value\"
'quoted \"value\"' = \"value\"
";
		var m = new TomlParser();
		var mm = m.loadFromString(quoted_keys); 
		Assert.equals(mm.getValue().get("quoted_keys").get("127.0.0.1"), "value");
		Assert.equals(mm.getValue().get("quoted_keys").get("character encoding"), "value");
		Assert.equals(mm.getValue().get("quoted_keys").get("key2"), "value");
		Assert.equals(mm.getValue().get("quoted_keys").get("quoted \"value\""), "value");
}

	function testArrays(){
		var arrays = "
[clients]
data = [ [\"gamma\", \"delta\"], [1, 2] ]

# Line breaks are OK when inside arrays
hosts = [
  \"alpha\",
  \"omega\"
]
"
;
		var m = new TomlParser(); 
		var mm = m.loadFromString(arrays);
		Assert.equals(""+ mm.getValue().get("clients").get("hosts"), "[alpha,omega]");
		Assert.equals(""+ mm.getValue().get("clients").get("data"), "[[gamma,delta],[1,2]]");

	}

	function testSubdivisions(){
		var subdivisions ="
[servers]

  # You can indent as you please. Tabs or spaces. TOML don\'t care.
  [servers.alpha]
  ip = \"10.0.0.1\"
  dc = \"eqdc10\"

  [servers.beta]
  ip = \"10.0.0.2\"
  dc = \"eqdc10\"
";
		var m = new TomlParser(); 
		var mm = m.loadFromString(subdivisions);
		
		Assert.isOfType(mm.getSectionByName("servers").getSectionByName("alpha"),TOMLSection);
		Assert.equals(mm.getValue().get("servers").get("alpha").get("ip"), "10.0.0.1" );
		Assert.isOfType(mm.getSectionByName("servers").getSectionByName("beta"),TOMLSection);
		Assert.equals(mm.getValue().get("servers").get("beta").get("ip"), "10.0.0.2" );
	}

	function testTypes(){
		var types ="
[database]
server = \"192.168.1.1\"
ports = [ 8001, 8001, 8002 ]
connection_max = 5000.5
enabled = true
";	
		var m = new TomlParser(); 
		var mm = m.loadFromString(types);
		Assert.isOfType(mm.getValue().get("database").get("server"), String);
		Assert.isOfType(mm.getValue().get("database").get("connection_max"), Float);
		Assert.isOfType(mm.getValue().get("database").get("enabled"), Bool);
		Assert.isOfType(mm.getValue().get("database").get("ports"), Array);
}



var testValidity = "key2 = # INVALID
first = \"Tom\" last = \"Preston-Werner\" # INVALID";


}
