package toml_parser;

import Date;

enum ValueType {
	VInt    ( v : Int );
	VFloat  ( f : Float );
	VString ( s : String );
	VBool   ( b : Bool );
	VArray  ( a : Array<ValueType> );
	VMap    ( m : Map<String, ValueType> );

	VDate   ( d : Date );
	VComment   ( s : String );
	VNull;
}

enum KeyType{
	KKey     ( s:String );
	KSection ( t:TOMLSection );
}

class TomlParser {
	public var commentsType = ["#",";"];
	public var keepComments:Bool   = true;
	public var globalName:String   = "GLOBAL";
	public var writeGlobalName:Bool  = false;
	public var strictTyping:Bool = false;

	public function new(){

	}

	public function loadFromString(text:String):TOMLSection{
		var tomlSectionName:String = globalName;
		var tomlSection = new TOMLSection(tomlSectionName);
		tomlSection.name = tomlSectionName;
		parseGeneralType(text, 0, tomlSection, tomlSection);

		return tomlSection;
	}

	public function loadFromData(m:Map<String, Dynamic>):TOMLSection{
		var tomlSectionName:String = globalName;
		var tomlSection = new TOMLSection(tomlSectionName);
		tomlSection.name = tomlSectionName;
		tomlSection.addValue(m);
		return tomlSection;

	}

	private function parseGeneralType(text:String, startPos:Int, tomlSection:TOMLSection, main:TOMLSection){

		var endLineSplitter = new EReg("\r\n|\r|\n","i");
		var endLinePos = 0;
		if (startPos >= text.length) return null;  // for utf8 and last line ????
		
		//this.match(s.substr(pos,len))
		if (endLineSplitter.match(text.substr(startPos + 1)))
		{
			endLinePos = endLineSplitter.matchedPos().pos+startPos+1;
			

			//if (endLinePos < startPos) endLinePos += 2;  // for utf8
			var endLineLen = endLineSplitter.matchedPos().len;
			//endLinePos += endLineLen;

			var equalPos = text.substring(startPos, endLinePos).indexOf("=");
			// check if equal sign before end of line
			if (equalPos != -1) //keySplitter.matchSub(text, startPos, endLinePos-startPos))
			{
				// Then it's a key
				var keyPos = startPos + equalPos; //keySplitter.matchedPos().pos;
				var key = parseKey(text.substring(startPos, keyPos));
				if (key == null) return parseGeneralType(text, endLinePos + endLineLen, tomlSection, main);
				var mv = checkMultilineValueType(text, keyPos + 1);
				if (mv == null) return parseGeneralType(text, endLinePos + endLineLen, tomlSection, main);
				var value = mv.value;
				tomlSection.addKey(text.substring(startPos, keyPos),value );
				return parseGeneralType(text, mv.pos, tomlSection, main);

			}
			else{

				var ss = text.substr(0, endLinePos).indexOf("[[", startPos);
				if (ss != -1){
					var a = text.substr(0, endLinePos).indexOf("]]", startPos);
					var name = text.substring(ss + 2,  a);
					tomlSection = main.getSectionByName(name, true, true);
					// should get section
					/*
					var lastIndex = name.lastIndexOf(".");
					if (lastIndex != "-1"){
						tomlSection = main.getSectionByName(name.substring(0,lastIndex));
					}
					tomlSection.addKey()*/
				}
				else{
					var ss = text.substr(0, endLinePos).indexOf("[", startPos);
					if (ss != -1){
						var a = text.substr(0, endLinePos).indexOf("]", startPos);
						var name = text.substring(ss + 1,  a);
						tomlSection = main.getSectionByName(name);
					}
				}
			}

			return parseGeneralType(text, endLinePos + endLineLen , tomlSection, main);

		}

		return null;

	}

	private function parseKey(key:String):String{
		key = StringTools.trim(key);

		if (strictTyping && key == "") {
			trace ("key cannot be empty");
			return null;
		}
		if (StringTools.startsWith(key, "\"") && StringTools.endsWith(key, "\"")){
			return (key.substring(1, key.length - 1));
		}
		if (StringTools.startsWith(key, "'") && StringTools.endsWith(key, "'")){
			return (key.substring(1, key.length - 1));
		}

		return key;
	}

	private function checkMultilineValueType(text:String, startPos:Int, ?addedSplitter:Array<String> ):{value:ValueType, pos:Int}{
		if (startPos >= text.length) return null;
		if (StringTools.isSpace(text, startPos)){
			return checkMultilineValueType(text, startPos+1);
		}


		if (StringTools.startsWith(text.substr(startPos),'"""')){
			var re = new EReg('(?:.*"?"?)[^' + '\\\\' + ']"""', "im");
			if (re.match(text.substr(startPos + 3))){
				var pos = re.matchedPos().pos + re.matchedPos().len + startPos;
				var s = text.substring(startPos + 3,pos);
				var r = ~/\\$\s+/gm;
				s = r.replace(s, "");
				s = StringTools.replace(s, '\\"','"');
				if (StringTools.startsWith(s,"\n")){
					return {value:VString(s.substring(1)), pos:pos + 3};
				}
				if (StringTools.startsWith(s,"\r\n")){
					return {value:VString(s.substring(2)), pos:pos + 3};
				}
				return {value:VString(s), pos:pos + 3};
				trace(text.substring(startPos + 3, pos));
			}

		}

		if (StringTools.startsWith(text.substr(startPos), "'''")){
			var re = new EReg("(?:.*'?'?)'''", "im");
			if (re.match(text.substr(startPos + 3))){
				var pos = re.matchedPos().pos + re.matchedPos().len + startPos;
				var s = text.substring(startPos + 3,pos);
				//trace(s);
				//var r = ~/\\$\s+/gm;
				//s = r.replace(s, "");
				//s = StringTools.replace(s, '\\"','"');
				if (StringTools.startsWith(s,"\n")){
					return {value:VString(s.substring(1)), pos:pos+3};
				}
				if (StringTools.startsWith(s,"\r\n")){
					return {value:VString(s.substring(2)), pos:pos+3};
				}
				return {value:VString(s), pos:pos+3};
				trace(text.substring(startPos+3,pos));
			}

		}

		if (text.charAt(startPos)==  '"'){
			var re = ~/^"([^"\\]*(?:\\.[^"\\]*)*)"/i;  // get starting quote
			if (re.match(text.substr(startPos))){
				//var pos = text.indexOf('"', re.matchedPos().pos+1);
				var pos = re.matchedPos().pos + re.matchedPos().len + startPos;
				var s = text.substring(startPos, pos);
				s = StringTools.replace(s, '\\"','"');
				var v = checkValueType(s);
				return {value: v, pos:pos};
			}
			var pos = text.indexOf('"', startPos+1);
			var v= checkValueType(text.substring(startPos, pos+1));
			return {value: v, pos:pos+1};
		}


		if (text.charAt(startPos)==  "'"){
			//trace(text.substr(startPos));
			var re= ~/^'(.*)'/i;  // get starting quote
			if (re.match(text.substr(startPos))){
				//var pos = text.indexOf('"', re.matchedPos().pos+1);
				var pos = re.matchedPos().pos + re.matchedPos().len + startPos;
				var s = text.substring(startPos, pos - 1);
				//s = StringTools.replace(s, '\\"','"');	
				//var v= checkValueType(s);
				return {value: VString(s.substring(1)), pos:pos};
			}
			var pos = text.indexOf('"', startPos + 1);
			var v= checkValueType(text.substring(startPos, pos + 1));
			return {value: v, pos:pos + 1};
		}

		if (text.charAt(startPos) == '['){
			var a = new Array<ValueType>();
			var pos = startPos + 1;
			while (text.charAt(pos) != ']'){
				var mv = checkMultilineValueType(text, pos, [",", "]"]);
				if (mv.value != null) a.push(mv.value);
				pos = mv.pos;
			}
			return {value:VArray(a), pos:pos+1};
		}

		if (text.charAt(startPos) == '{'){
			var m:Map<String, ValueType> = new Map();
			var pos = startPos + 1;
			var eqPos:Int =0 ;
			while ( text.charAt(pos) != '}'){
				if (StringTools.isSpace(text,pos))
				{pos = ++pos; continue;}
				eqPos =  text.indexOf("=", pos);
				if (eqPos == -1) break;
				var keyvalue = parseKey(text.substring(pos,eqPos));
				var mv2 = checkMultilineValueType(text, eqPos+1, [",", "}"]);
				var vvalue = mv2.value;
				m.set(keyvalue, vvalue);
				pos = mv2.pos;
				if (text.charAt(pos) == ",") pos +=1;
			}
			return {value:VMap(m), pos:pos+1};
		}

		if (text.charAt(startPos) == "#"){
			var sp = "\r\n|\r|\n";
			var splitter = new EReg(sp,"i");
			splitter.matchSub(text, startPos);
			var endLine:Int=  splitter.matchedPos().pos;
			var v = checkValueType(text.substring(startPos,  endLine));
			return {value:v, pos:endLine};


		}

		var sp = "\r\n|\r|\n|#|,|]";
		var splitter = new EReg(sp,"i");
		var endLine:Int =  splitter.matchSub(text, startPos) ? splitter.matchedPos().pos : -1;
		if (endLine > 0){
			
			var v = checkValueType(text.substring(startPos,  endLine));
			//endLine +=splitter.matchedPos().len;

			//if ( v!=null){
				
				if (text.charAt(endLine) == "\n" || text.charAt(endLine) == "\r" || text.charAt(endLine) == " "){
				
					return  {value:v, pos:endLine +splitter.matchedPos().len};
				}
				else if (text.charAt(endLine) == ","){
				
					return  {value:v, pos:endLine +splitter.matchedPos().len};
				}
				else if (text.charAt(endLine) == "]"){
				
					return  {value:v, pos:endLine};
				}
				else if (text.charAt(endLine) == "#"){
				
					return  {value:v, pos:endLine};
				}
					
					
				else
					return  {value:v, pos:endLine+splitter.matchedPos().len};
			//}
		}
		
/*
		if (strictTyping){
			trace("problem with String at pos" + startPos +" , has undefined value");
			return null;
		}*/

		return {value:null, pos:startPos+1};
	}

	private function checkValueType(v:String):ValueType{
		v = StringTools.trim(v);
			//101
		function convertBinaryString(s:String):Int{
			var i =  s.length;
			var num = 0;
			while (i > 0){
				i -= 1;
				if (s.charAt(i) == "1" ) num = num | (1 << (i+1));
				//if (s.charAt(i) == "0" ) num = num | (0 << i);
			}
			return num;
		}

		function convertOctalString(s:String):Int{
			var i =  s.length;
			var ii = 0;
			var num = 0;
			while (i > 0){
				i -= 1;
				var n = (Std.parseInt(s.charAt(i)));
				if (n == null) return 0;
				num = num | (n << ii);
				ii += 3;
			}
			return num;
		}

		if (StringTools.startsWith(v, '"')){
			if (v.charAt(v.length - 1) != '"') return null;
			return VString(v.substring(1, v.length - 1));
		}
		if (StringTools.startsWith(v, '#')){
			return VComment(v.substring(1,v.length));
		}
		if (v == "true"){
			return VBool(true);
		}

		if (v == "false"){
			return VBool(false);
		}

		if (v == "null"){
			return VNull;
		}

		v = StringTools.replace(v, "_", "");

		if (StringTools.startsWith(v, "0b")){
			return VInt(convertBinaryString(v.substr(2)));
		}
		if (StringTools.startsWith(v, "0o")){
			return VInt(convertOctalString(v.substr(2)));
		}
		if (StringTools.startsWith(v, "0x")){
			return VInt(Std.parseInt(v));
		}

		var dReg = ~/\d\d\d\d-\d\d-\d\d/;
		if (dReg.match(v)){
			return VDate(Date.fromString(v));
		}

		var f = Std.parseFloat(v);
		if ((!Math.isNaN(f))) {
			return VFloat(f);
		}

		var i = Std.parseInt(v);
		if (i != null){
			return VInt(i);
		}

		if (v == "nan"){
			return VFloat(Math.NaN);
		}
		if (v == "+nan"){
			return VFloat(Math.NaN);
		}
		if (v == "-nan"){
			return VFloat(-Math.NaN);
		}

		

		return null;
	}


}

class TOMLSection {
	public var subKeys:Array<KeyType> = new Array();

	public var map:Map<String,ValueType> = new Map();
	public var name:String = "";

	public var isTable:Bool = false;

	public var type:Bool = false; // false  is [General.example]  type ,  true is General.example = ""

	public function new(s:String){
		name = s;
	}

	public function addKey(k:String, v:ValueType, isString = false){
		var s=StringTools.trim(k);
		var re= ~/^"([^"\\]*(?:\\.[^"\\]*)*)"/i;  // get starting quote
		var re2= ~/^'(.*)'/i;
		var dotPos=-1;
		if (re.match(s)){
			dotPos = s.indexOf(".", re.matchedPos().len);
			s = re.matched(1);
		}
		else if (re2.match(s)){
			dotPos = s.indexOf(".", re2.matchedPos().len);
			s = re2.matched(1);
		}
		else{
			dotPos = s.indexOf(".");
		}

		if (!isString && (dotPos != -1)){
			var par = StringTools.trim(s.substring(0, dotPos));
			var t = getSectionByName(par);//, type);
			//TomlParser
			t.addKey(StringTools.trim(s.substring(dotPos + 1)), v);
		}
		else{
			var kk = KKey(s);
			map.set(s,v);
			subKeys.push(kk);
		}
	}

	public function getSectionsNames():Array<String> {
		var a:Array<String> = [];
		for (k in subKeys){
			switch (k){
				case (KSection(t)) :
					a.push(t.name);
				default:
					continue;
			}
		}
		return a;
	}

	public function getSectionByName(s:String, doCreate:Bool = true, createAsTable = false ,
									accessLastRowOfTable = false) :TOMLSection{

		var s      = StringTools.trim(s);
		var re     = ~/^"([^"\\]*(?:\\.[^"\\]*)*)"/i;  // get starting quote
		var re2    = ~/^'([^'\\]*(?:\\.[^'\\]*)*)'/i;
		var dotPos = -1;
		if (re.match(s)){
			dotPos = s.indexOf(".", re.matchedPos().len);
			s = re.matched(1);
		}
		else if (re2.match(s)){
			dotPos = s.indexOf(".", re2.matchedPos().len);
			s = re2.matched(1);
		}
		else{
			dotPos = s.indexOf(".");
		}

		if ((dotPos != -1)){
			var par = StringTools.trim(s.substring(0, dotPos));
			return getSectionByName(par, doCreate, false, true)
					.getSectionByName(StringTools.trim(s.substring(dotPos + 1)), doCreate, createAsTable);
		}
		for (sec in subKeys){
			switch (sec) {
				case (KKey(v)) :
					if (v == s) return null;
				case (KSection(t)) :
					if (t.name == s) {

						if (t.isTable && accessLastRowOfTable) {
							switch (t.subKeys[t.subKeys.length - 1]){
								case (KSection(t)): return t;
								default:
							}
						}

						if (doCreate && createAsTable){
							var section = new TOMLSection(s);
							//section.isTable = true;
							t.subKeys.push(KSection(section));
							return section;
						}


						return t;

					} 
			}
		}

		if (doCreate){
			var t = new TOMLSection(s);
			subKeys.push(KSection(t));
			if (createAsTable) {
				var section = new TOMLSection(s);
				t.subKeys.push(KSection(section));
				t.isTable = true;
				return section;
			}
			return t;
		}
		else{
			return null;
		}
	}

	public function prettyPrint():String{
		var s = "\r\n";
		trace(map);
		if (name != "")  s += "[" + name + "]\r\n";

		for (sec in subKeys){
			trace(sec);
			switch (sec) {
				case (KKey(v)):
					s += v + " = " ;
					switch (map.get(v)){
						case (VInt(i))   : s += i;
						case (VFloat(f)) : s += f;
						case (VString(ss)) :  s += '"' + ss + '"';
						case (VBool(b)) :
							if (b)  s += "true";
							else    s += "false";
						case (VArray(a)):
							s += a.map(item -> dynamicValueForValueType(item));
						case (VMap(m)):
							s += "{ ";
							var first = true;
							for ( k => v in m){
								if (!first) s += ", "  else first = false;
								s += k + "=" + TOMLSection.dynamicValueForValueType(v);
							}
							s += "}";
						case (VComment(s)) : s += "#Comment";
						case (VDate(d)) : s+=d.getFullYear()+"-"+d.getMonth()+"-"+d.getDay();
						case VNull : s += "null";
					}
					s += "\r\n";
				case (KSection(t)) :
					s += "\r\n";
					s += t.prettyPrint();
			}
		}
		return s;
	}

	public function toString():String{
		//var s = prettyPrint();
		var s = "[" + name + "]";
		return s;
	}
	public function parseString(){

	}

	public function addValue(m:Map<String, Dynamic>){
		for (k => v in m){
			if (#if (haxe >= "4.1.0") Std.isOfType #else Std.is #end(v,haxe.ds.StringMap)){
				var t = getSectionByName(k);
				t.addValue(v);
			}
			else{
				map.set(k, changeIntoKValue(v));
				subKeys.insert(0,KKey(k));
			}
		}
	}

	public  function changeIntoKValue(d:Dynamic):ValueType{
		switch (Type.typeof(d)){
			case TInt    : return VInt(d);
			case TFloat  : return VFloat(d);
			case TClass(String) : return VString(d);
			case TBool   : return VBool(d);
			case TClass(Array)  : return VArray(d.map(item -> changeIntoKValue(item)));
			case TClass(haxe.ds.StringMap)    : { return VMap(d); }
			case null  : return VNull;
			default : return null;
		}

		return VNull;
	}


	public function getValue():Dynamic {
		if (isTable){
			var a:Array<Map<String, Dynamic>> = [];
			var lastSection:TOMLSection = null;
			for (sec in subKeys){
				switch (sec) {
					case (KSection(t)) :
						//if (t.name == name) {
							a.push(t.getValue());
							lastSection = t;
						//}

						
						
					default:
				}
			}
			return a;
		}
		var m:Map<String, Dynamic> = new Map();

		for (sec in subKeys){
			switch (sec) {
				case (KKey(v)):
					var key   = v;
					if (map.get(key) == null) continue;
					var value:Dynamic = TOMLSection.dynamicValueForValueType(map.get(key));
					m.set(key, value);
				case (KSection(t)) :
						m.set(t.name, t.getValue());
				default :
				// do nothing
			}
		}
		return m;
	}

	public static function dynamicValueForValueType(value:ValueType):Dynamic{
		switch (value){
			case (VInt(i))    : return i;
			case (VFloat(f))  : return f;
			case (VString(s)) : return s;
			case (VBool(b))   : return b;
			case (VDate(d))   : return d;
			case (VArray(a))  : var v = a.map(item -> dynamicValueForValueType(item)); return v;
			case (VMap(m))    : 
				var ma:Map<String, Dynamic> = new Map();
				for ( k => v in m){
					ma.set(k, dynamicValueForValueType(v) );
				}
				return ma;
			case VNull        : return null;
			default           : return null;
		}
		return null;
	}
}
